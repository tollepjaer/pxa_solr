<?php
if (!defined('TYPO3_MODE')) {
    die('Access denied.');
}

unset($GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['tslib/class.tslib_fe.php']['hook_eofe']['workspaces']);

Tx_Extbase_Utility_Extension::configurePlugin(
    $_EXTKEY,
    'Searchbox',
    array(
        'Search' => 'searchbox, quicksearch'
    ),
    // non-cacheable actions
    array(
    'Search' => 'searchbox'
    )
);

Tx_Extbase_Utility_Extension::configurePlugin(
    $_EXTKEY,
    'Searchresult',
    array(
        'Search' => 'result, getSuggestResults'
    ),
    // non-cacheable actions
    array(
        'Search' => 'result, getSuggestResults'
    )
);
