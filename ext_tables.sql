#
# Table structure for table 'tx_pxasolr_searchstat'
#
CREATE TABLE tx_pxasolr_searchstat (
  uid int(11) NOT NULL auto_increment,
  pid int(11) DEFAULT '0' NOT NULL,
  tstamp int(11) unsigned DEFAULT '0' NOT NULL,
  crdate int(11) unsigned DEFAULT '0' NOT NULL,
  deleted tinyint(3) unsigned DEFAULT '0' NOT NULL,
  hidden tinyint(4) unsigned DEFAULT '0' NOT NULL,
	searchword varchar(128) DEFAULT '' NOT NULL,
  category varchar(128) DEFAULT '' NOT NULL,
  most_recent_amount int(11) DEFAULT '0' NOT NULL,
  performed_searches int(11) DEFAULT '0' NOT NULL,  

	PRIMARY KEY (uid),
  UNIQUE KEY searchword_cat (searchword,category)
) ENGINE=InnoDB;