jQuery(document).ready(function(){	

	jQuery('.pxasolr_typeahead_qs, .pxasolr_typeahead').typeahead({
		hint: false,
        highlight: true,
        minLength: 2
	},{
		name: 'quicksearch',
		displayKey: 'value',
		source: function (query, cb) {
			jQuery.post(suggest_url, 
			{
				termLowercase: query.toLowerCase(),
				termOriginal: query
			},
			function(data) {
				var output = [];
				jQuery.each(data, function(term, termIndex) {
					output.push({ value: term, count:  data[term]});
				})
				cb(output);
			});
		},
		templates: {
			empty: Handlebars.templates['empty'],
			suggestion: Handlebars.templates['suggestion']
		}
	});

	jQuery('.pxasolr_typeahead_qs').bind('typeahead:selected', function(obj, data, name) {
		if (data.value.length > 0) {
			window.document.location = jQuery('.pxasolr_typeahead_form').attr('action') + '#' + data.value;
		}
	});

	jQuery('.pxasolr_typeahead_qs').keypress(function(event) {
		if (event.which == 13) {
			event.preventDefault();
			if (this.value.length > 0) {
     	   		window.document.location = jQuery('.pxasolr_typeahead_form').attr('action') + '#' + this.value;
     	   	}
		}
	});

	jQuery('.pxasolr_typeahead').bind('typeahead:selected', function(obj, data, name) {
		if (data.value.length > 0) {
			window.document.location = jQuery('.search-result-form').attr('action').replace(/[\?&]type(.*)/,"")+'#'+ encodeURIComponent(data.value);	
		}
	});

	jQuery('.pxasolr_typeahead').keypress(function(event) {
		if (event.which == 13) {
			event.preventDefault();
			if (this.value.length > 0) {
     	   		window.document.location = jQuery('.search-result-form').attr('action').replace(/[\?&]type(.*)/,"")+'#'+ encodeURIComponent(this.value);	
     	   	}
		}
	});

});