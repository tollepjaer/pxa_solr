jQuery(document).ready(function(){    
    var lastSword = '';
	var loaderSuggestResult = false; 

	jQuery('.search-result-input-sword').keyup(function (e){
		var keyCode = e.keyCode ? e.keyCode : e.which;	
		var sword = $(this).val();

		if(keyCode == 40) {
			$('#suggestResult:visible .suggest-result-link').first()
				.addClass('focused')
    			.focus();
		} else {
			setTimeout(function() { 
	        if($('.search-result-input-sword').val() == sword && sword.length >= 3 && sword != lastSword && loaderSuggestResult == false){
	        		$('#suggestResult').show();
	        		$('#suggestResult').html(searchingSuggestResult);
	        		loaderSuggestResult = true;

		            $.ajax({
		                type: "POST",
		                url: suggest_result_url + "?type=1319071",//+sword,
		                data: 'tx_pxasolr_searchresult%5Bsword%5D=' + sword,
		                success: function(data) {
		                	if(data != 'null') {
			                    var obj = $.parseJSON(data);
			                    lastSword = sword;
								$('#suggestResult').html(obj.html);
								loaderSuggestResult = false;
							} else {
								$('#suggestResult').html(search_empty);
							}
		                }
		            })
		        }
		    }, 1000);
		}

	});
	$('body').click(function(){
		$('#suggestResult').hide();
	});
	$('#suggestResult, input.search-result-input-sword').click(function(e) {
		if (e.stopPropagation) {
	        e.stopPropagation();
	    }
    });
    $('#suggestResult .suggest-result-link')
    	.live('mouseover',function(){
    		$('#suggestResult .suggest-result-link.focused').removeClass('focused');
    		$(this).addClass('focused');
    		$(this).focus();
    	});


    $('#suggestResult').live('keydown',function(e){
    	var keyCode = e.keyCode ? e.keyCode : e.which;
    	var focused = $('#suggestResult .suggest-result-link.focused');

    	if(keyCode == 38) {
    		
    		if(focused.prev().length != 0) {
    			if(focused.prev().prop('tagName') == 'A') {
    				focused.prev()
    					.addClass('focused')
    					.focus();
    				focused.removeClass('focused');
    			}
    			else if(focused.prev().prev().length != 0) { 
    				focused.prev().prev() 
    					.addClass('focused')
    					.focus();
    				focused.removeClass('focused');
    			}
    		} else {
    			return false;
    		}
    	} else if(keyCode == 40) {
    		
    		if(focused.next().length != 0) {
    			if(focused.next().prop('tagName') == 'A') {
    				focused.next()
    					.addClass('focused')
    					.focus();
    				focused.removeClass('focused');
    			}
    			else if(focused.next().next().length != 0) {
    				focused.next().next() 
    					.addClass('focused')
    					.focus();
    				focused.removeClass('focused');
    			}

    		} else {
    			return false;
    		}    		
    	} else if(keyCode == 13) {
    		return true;
    	}
    	return false;
    });

});