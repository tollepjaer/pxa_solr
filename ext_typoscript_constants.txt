tx_pxasolr_searchbox {
	# cat=content/mocsearch/resultpid; type=string; label= Result Page ID: The page ID of the page containing the search result.
	resultPid =
  # cat=content/mocsearch/rememberhistory; type=string; label= Enabled by default. Decides if we should update hashes. Disable this when using solr with jquery mobile.
  rememberHistory = 1
}

tx_pxasolr_pi2 {
	# cat=content/mocsearch/jsonpagetype; type=string; label= JSON page type: The page type for the JSON results.
	jsonPageType = 67
	
	# cat=content/mocsearch/imagewidth; type=string; label= Default image width.
	imageWidth = 75c
	
	# cat=content/mocsearch/imageheight; type=string; label= Default image height.
	imageHeight = 75c
	
	# cat=content/mocsearch/dateformat; type=string; label= Default date format.
	dateFormat = d.m.Y
}