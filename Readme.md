# TYPO3 Extension "pxa_solr"
## Changelog

### 3.0.0.2
* Prev/next links are hidden when they are not needed.

### 3.0.0.1

* Url fixes, now uses uriBuilder, new variables to templates
	* Custom templates needs to be updated with new variables.
* Suggest also make use of filters from ts.
	* When writing filters, note that suggest implements filters from all "searchTag:s".

### 3.0.0
-	Template overrides, typoscript now uses templateRootPath**s**, layoutRootPath**s** and partialRootPath**s**.
-		We should now move partials for our extensions to the actual extension, and add an override to a new number in partialRootPaths.
-		Solr typoscript configuration for our extensions should also be in a static include.
-	Search statistics have model and repository now.
-	Stores search statistics in repository now instead of direct query.
-	Search statistics is now visible as a list in BE.
-	Only creates statistics if a storagePid is set.
-	Requires TYPO3 version 6.2.x
-	Requires at least solr version 3.0.0

### 0.9.8
