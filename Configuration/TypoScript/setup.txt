page.includeCSS.pxasolr = typo3conf/ext/pxa_solr/Resources/Public/Css/pxasolr.css

page.includeJSFooterlibs {
	mocSearch = EXT:pxa_solr/Resources/Public/Scripts/moc_search.js
}

[globalVar = LIT:1 = {$plugin.tx_pxasolr.suggestResults.enable}]
	page.includeJSFooterlibs.suggest = EXT:pxa_solr/Resources/Public/Scripts/suggest_result.js
[else]
	page.includeJSFooterlibs.suggest = EXT:pxa_solr/Resources/Public/Scripts/suggest.js
[global]

[globalVar = LIT:1 = {$plugin.tx_pxasolr.jqueryUi}]
	page.includeJSFooterlibs.suggest = EXT:pxa_solr/Resources/Public/Scripts/suggest_jquery-ui.js
[else]
		# Change to use typeahead.jquery.js instead since bootstrap typeahead is removed from bootstrap3
	page.includeJSFooterlibs {
		handlebars = EXT:pxa_solr/Resources/Public/Scripts/handlebars.runtime-v1.3.0.js
		typeaheadJquery = EXT:pxa_solr/Resources/Public/Scripts/typeahead.jquery.min.js
		templates = EXT:pxa_solr/Resources/Public/Scripts/handlebars-templates.js
		typeahead = EXT:pxa_solr/Resources/Public/Scripts/typeahead.js
		suggest >
	}
	#page.includeJSFooterlibs.bootstrapTypeahead = EXT:pxa_solr/Resources/Public/Scripts/bootstrap-typeahead.min.js
[global]


plugin.tx_pxasolr {
	view {
		templateRootPaths {
			default = {$plugin.tx_pxasolr.view.templateRootPath}
		} 
		partialRootPaths {
			default  = {$plugin.tx_pxasolr.view.partialRootPath}
		}
		layoutRootPaths {
			default = {$plugin.tx_pxasolr.view.layoutRootPath}
		}
	}
	persistence {
		storagePid = {$plugin.tx_pxasolr.persistence.storagePid}
	}
	settings {
		aimpointProductSinglePid = {$plugin.tx_pxasolr.aimpointProductSinglePid}

		enableImages = 1
		contentImagesPriority = 0
		enableMediaImages = 1
		enableContentImages = 1
		spellChecker = 1

		suggestResults {
			enable = {$plugin.tx_pxasolr.suggestResults.enable}
			limit = {$plugin.tx_pxasolr.suggestResults.limit}

			# Sorting
			sorting = tx_pxaaimpoint_products,tt_news,pages

			allowedSuggestSearchResult {
				tx_pxaaimpoint_products = {$plugin.tx_pxasolr.suggestResults.allowedSuggestSearchResult.tx_pxaaimpoint_products}
				tx_news_domain_model_news = {$plugin.tx_pxasolr.suggestResults.allowedSuggestSearchResult.tx_news_domain_model_news}
				pages = {$plugin.tx_pxasolr.suggestResults.allowedSuggestSearchResult.pages}
			}
		}

		quicksearch {
			form {
				class = form-inline
			}
			input {
				class = form-control
				size = 14
			}
			submit {
				class = btn btn-default
				enabled = 1
			}
		}
		searchbox {
			form {
				class = 
			}
			input {
				class =
				size = 14
			}
			submit {
				class = 
			}
		}
		searchresult {
			container {
				class = 
			}
			searchtagmenu {
				class = 
				item {
					count {
						prefix = (
						suffix = )
						class =
					}
				}
			}
			pagination {
				class =
			}
			info {
				class = 
			}
		}
	}
}

plugin.tx_pxasolr._CSS_DEFAULT_STYLE (
	.tx_mocsearch_pi1 .searchtag-menu.init,
	.tx_mocsearch_pi1 .search-overview,
	.tx_mocsearch_pi1 .search-result,
	.tx_mocsearch_pi1 .searchtag-result {
		display: none;
	}
	
	.searchtag-menu {
		padding-top: 10px;
	}
	
	.searchtag-menu ul {
		list-style: none;
		padding: 0;
		margin: 0;
	}
	
	.searchtag-menu ul li {
		display: inline;
		margin-right: 10px;
	}
	
	.search-overview ul {
		list-style: none;
		margin: 0;
		padding: 0;
	}
	
	.search-result ul {
		list-style: none;
		margin: 0;
		padding: 0;
	}

	.pagination a {
		color: #000;
	}
	
	.pagination a.act {
		font-weight: bold;
	}
	
	.highlighted {
		background-color: yellow;
	}
)
