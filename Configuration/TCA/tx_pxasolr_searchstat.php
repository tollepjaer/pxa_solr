<?php
if (!defined('TYPO3_MODE')) {
    die('Access denied.');
}

$ll = 'LLL:EXT:pxa_solr/Resources/Private/Language/locallang_db.xlf:';

return array(
    'ctrl' => array(
        'title' => $ll . 'label.title',
        'label' => 'searchword',
        'label_alt' => 'performed_searches',
        'label_alt_force' => 1,
        'tstamp' => 'tstamp',
        'crdate' => 'crdate',
        'cruser_id' => 'cruser_id',
        'dividers2tabs' => false,
        'default_sortby' => 'ORDER BY performed_searches DESC',
        'delete' => 'deleted',
        'enablecolumns' => array(
            'disabled' => 'hidden',
        ),
        'iconfile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath('pxa_solr') . 'ext_icon.gif',
        'searchFields' => 'uid,searchword',
        'hideTable' => false,
    ),
    'interface' => array(
        'showRecordFieldList' => 'hidden,searchword,category,most_recent_amount,performed_searches'
    ),
    'columns' => array(
        'hidden' => array(
            'exclude' => 0,
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.hidden',
            'config' => array(
                'type' => 'check',
                'default' => 0
            )
        ),
        'searchword' => array(
            'exclude' => 0,
            'label' => $ll . 'label.searchword',
            'config' => array(
                'type' => 'input',
                'size' => 30,
                'readOnly'=>1,
            )
        ),
        'category' => array(
            'exclude' => 0,
            'label' => $ll . 'label.category',
            'config' => array(
                'type' => 'input',
                'size' => 30,
                'readOnly'=>1,
            )
        ),
        'most_recent_amount' => array(
            'exclude' => 0,
            'label' => $ll . 'label.most_recent_amount',
            'config' => array(
                'type' => 'input',
                'size' => 10,
                'readOnly'=>1,
            )
        ),
        'performed_searches' => array(
            'exclude' => 0,
            'label' => $ll . 'label.performed_searches',
            'config' => array(
                'type' => 'input',
                'size' => 10,
                'readOnly'=>1,
            )
        ),
    ),
    'types' => array(
        0 => array(
            'showitem' => 'hidden,searchword,category,most_recent_amount,performed_searches',
        )
    ),
    'palettes' => array(
        '1' => array(
            'showitem' => 'searchword,hidden,category,most_recent_amount,performed_searches',
        ),
    )
);
